# jupyter-decipher

Mini python script that converts jupyter files to python files so you don't have to constantly copy-paste information from a good IDE and that browser abomination.

## How to use:
- **Linux & MacOS**
	- Clone the repository `git clone https://gitlab.com/El-Albatross/jupyter-decipher.git` to your directory of choice
	- Run `python/jupyter-decipher.py filename.ipynb` in any shell
	- Open the new Python file in your text editor of choice
- **Windows**
	- Clone the repository (see above)
	- open the python directory in Powershell or the Python shell I guess
	- run `jupyter-decipher.py filename.ipynb`

## Some examples

You can run the bash script in `examples` on a folder to bulk convert all `.ipynb` files in a directory.

I mapped a keybinding in `lf` (a file manager) and can bulk convert my jupyter files into python files in a click.

![example](decipher.png)


## This project will be ported to Rust very soon
